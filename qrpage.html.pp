#lang pollen
◊(require xml xml/path pollen/template/html racket/path)
◊(define qr-path (make-wifi-qr))

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>WiFi QR Code! WiFi QR Code! WiFi QR Code!</title>
        <style type="text/css">
            @font-face {
                font-family: 'IBM Plex Mono';
                font-style: normal;
                font-weight: 400;
                src: url("IBMPlexMono-Regular.woff2") format("woff2");
            }
            @font-face {
                font-family: 'IBM Plex Mono';
                font-style: normal;
                font-weight: 700;
                src: url("IBMPlexMono-Bold.woff2") format("woff2");
            }
           

            ◊(define extra-margin (/ (width) 20.0))
            ◊;{
               simple-qr adds padding between outside of code and image border
               it's 4 modules (QR pixels) on each side
               https://github.com/simmone/racket-simple-qr/blob/62f253a33334ccf9333792309336d451da479c84/simple-qr/share/draw/svg.rkt#L24
               So we could use that info... or munge the svg ourselves... but this is more fun :)
               Assumptions (verified):
                 - QR margin is the same on all sides
                 - SVG is a square with equal width and height
                 - All modules are located at xpath '(svg symbol use)
                 - Nothing other than modules is at that location
               This can't be static because the length of the SSID and pass
               determine how many modules the QR code has.
               For a good-enough approximation, use (* qr-image-width 0.075).
            }
            ◊(define qr-image-width (- (width) (* 2 extra-margin)))
            ◊(define embedded-qr-margin-inches
              (let* ([svg-data (xml->xexpr (read-xml/element (open-input-file qr-path)))]
                     [qr-base-pixels (string->number (se-path* '(svg #:width) svg-data))]
                     [qr-start (apply min (map string->number (se-path*/list '(svg symbol use #:y) svg-data)))]
                     [margin-ratio (/ qr-start qr-base-pixels)])
                (* margin-ratio qr-image-width)))

            body {
                width: ◊(width)in;
                border: 0.2em dashed black;
                display: grid;
                font-family: IBM Plex Mono;
                font-size: ◊(* 0.075 (width))in;
                text-align: left;
                margin: 0.5in;
            }

            wifi-meta {
                display: grid;
                row-gap: 1em;
                margin: ◊(+ extra-margin embedded-qr-margin-inches)in;
                margin-bottom: 0;
            }

            wifi-pass { align-self: end; }

            key { font-weight: bold; }
            value {
                background: lightgray;
                padding-inline: 0.2em;
                display: inline-block;
                word-break: break-word;
            }

            img {
                width: ◊|qr-image-width|in;
                margin: ◊|extra-margin|in;
            }
        </style>
    </head>
    ◊(define display-pass (cond
                            [(string=? "" (password)) "No Password"]
                            [else (password)]))
    ◊(->html (body (wifi-meta
                     (wifi-ssid (key "WiFi Network:") (br) (value (ssid)))
                     (wifi-pass (key "Password:") (br) (value display-pass)))
                   (img #:src (path->string (file-name-from-path qr-path)))))
</html>