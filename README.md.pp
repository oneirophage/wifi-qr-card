#lang pollen
# WiFi QR Card
◊(require "main.rkt")
Racket CLI script that generates a printable HTML card with your WiFi SSID,
password, and a QR code to scan to join the network.

<img src="example.png" alt="Screenshot of a card with a dashed border, and the text:
WiFi Network: Place of Honor
Password: the literal number 5
The values for network name and password have a light grey highlight.
Underneath is a black and white QR code." width="431" height="696"/>

## Install and Use

1. [Install Racket](https://github.com/racket/racket/wiki/Installing-Racket)

1. `raco pkg install https://codeberg.org/oneirophage/wifi-qr-card.git`

1. ` ◊invocation --ssid 'Place of Honor' --password 'the literal number 5'`
   * If you don't want your WiFi password in your shell history, prefix the
     command with a space. Or:
     * Bash: `read -p 'pass: '; ◊invocation -s 'Place of Honor' -p "$REPLY"`
     * Zsh: `read '?pass: '; ◊invocation -s 'Place of Honor' -p "$REPLY"`
     * Fish: `◊invocation -s 'Place of Honor' -p (read -P "pass: ")`

1. A web browser with the generated card should open. Print the page on
   cardstock or other heavy paper and cut it out. Ensure "Print Backgrounds"
   is set while printing.

1. To remove the password from your system (the generated files and the render
   cache), run `◊invocation --clean`, or `raco pkg remove` the package.
   You may also want to remove the commands from your shell history.

This goes well in a [folded pocket][simple], so you can pull it out and hand it
to guests.
* If you do that, and are working with US Letter paper, set `-w 2.6`.
  For A4, try `-w 2.8`.

What I've done is printed a second copy of the card, taken the bottom half,
and glued it onto the pocket, like so (this is not as clean as the real version,
but I can't exactly show off the password):

<img src="in_pocket.jpeg" alt="Photo of QR card in a folded pocket, with a QR
code overlayed across it. This makes the QR code visible, instead of occluded"
width="514" height="682.667">
<img src="out_pocket.jpeg" alt="Photo of QR card next to the pocket, where you
can still scan the code if needed" width="514" height="682.667">

This lets you mount the pocket on the wall, so people can scan the code
with their phones without doing taking the card out, and you can still hand
the card to guests who are sitting down.

(I admit the above is not the prettiest, and am just going to say that design
is not one of my strengths. If you make it better, show me :) )

## Full Usage Details

◊; this is so fucking cool
```console
◊(let ([s (open-output-string)])
    (parameterize ([current-output-port s])
      (main #("--help")))          
    (get-output-string s))```

## Contributing

* `raco test .` should render everything, and tests should pass
* `raco pollen start` and then opening `qrpage.html` should show a page with
  stub values
* `◊invocation -s ssid -p pass` should render correctly
* Don't edit the `README.md`, edit the `README.md.pp` and render that
  (`raco pollen render README.md`)
* Please run `Racket -> Reindent All` in DrRacket when changing code


## Other Notes

Q: Why HTML as the output?

A: I didn't want to make people install a texlive distribution. Everyone has
a browser, though.

Q: Why [Pollen][pollen]?

A: The lozenge character (◊"◊") is cool as fuck. More seriously, I stumbled into
the project when looking for a static site generator, and was really enamored by:

* The seamless interleaving of code and data
* No template language, only the full Racket language (I hate fighting with
  limited, language-specific, not-otherwise-useful templating languages)
* The ideas of semantic markup, and multiple output targets (I've released a
  magazine before as an ePub and PDF, and it would've been SO much better to
  have one source that generated both)
* The general utility of a template language that can be used for almost
  anything (unlike a static site generator, I know what I learn here will be
  useful down the road for non-website things)
* The relative simplicity of Pollen, compared to other SSGs or things that can
  be used as SSGs (I'd rather build a small pagination module myself than pull
  in a dependency tree that'll break in three years when I go to update
  something)
* The lozenge character really is cool as fuck

Q: Why is the code like that?

A: This is a project I made to learn Racket. It's the first time I've done
anything non-trivial in a Lisp. Tell me how to do better!

[simple]: https://www.youtube.com/watch?v=zsAtwgB5Bh4&t=250s
[pollen]: https://docs.racket-lang.org/pollen/index.html