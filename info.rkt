#lang info
(define collection "wifi-qr-card")
(define pkg-desc "Generate a printable card with a WiFi QR code")
(define version "1.0")
(define deps '("base" "pollen" "simple-qr"))
(define build-deps '("rackunit-lib"))
(define update-implies '("pollen" "simple-qr"))
(define pkg-authors '("august" "september"))
; https://lifning.neocities.org/BBHL
(define license '(BigBillHells))